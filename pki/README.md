# SSL/TLS & Public Key Infrastructure

## Certificate Authorities and Chaining
In a Public key Infrastructure (**PKI**), a **Certificate Authority (CA)** is either a third party trusted to verify the identity of an entity (such as an user or domain), or an externally-signed CA certificate signed by a CA owned and operated by the same organization. The design of a Certificate Authority architecture usually includes several layers of CAs, forming a chain. At the top of the chain, is the **Root CA**. The Root CA has signed its own certificate, and this certificate is the one that gets embedded in web browsers as trusted authority. Below the Root CA are the **Intermediate CAs**. The Intermediate CA certificates are signed by the Root CA, and may or may not issue certificates to endpoints.

When a user navigates to a TLS-protected website, the server certificate is verified against the CA that signed it, and the chain of CAs from the signing CA up to a trusted Root CA. If the chain does not contain a trusted CA, the website is not trusted and a warning is displayed.
```
+--------------------+
| Site Certificate   |
|====================|
| Owner's Name       |
|--------------------|                +--------------------+
| Owner's Public Key |                | Intermediate Cert  |
|--------------------|                |====================|
| Issuer's (CA) Name |  Issuer -- >   | Owner's Name       |
|--------------------|                |--------------------|                +-----------------+
| Issuer's Signature | < -- Verifies  | Owner's Public Key |                | Root CA Cert    |
+--------------------+                |--------------------|                |=================|
                                      | Issuer's (CA) Name |  Issuer -- >   | Root Name       |
                                      |--------------------|                |-----------------|
                                      | Issuer's Signature | < -- Verifies  | Root Public Key |
                                      +--------------------+                |-----------------|
                                                                            | Root Signature  |
                                                                            +-----------------+
```
## Certificates
A certificate is but a simple digital document which contains a public key and some information about the entity associated with it along with a digital signature from the certificate issuer. Certificates became the basis of PKI.

### ASN.1, DER and PEM
Usually, certificates follow the **X.509** standard, that is encoded or digitally signed according to RFC 5280. X.509 relies on **Distinguished Encoding Rules** (**DER**) which allows only one way to encode **ASN.1** values, which are a set of rules that support definition, transport and exchange of complex data. This is really important to digital signatures.

**PEM** (**Privacy-Enhanced Mail**) is an ASCII encoding of DER using Base64 encoding.

Most of the certificates are supplied in the **PEM** format, but sometimes it's possible to encounter **DER**. Sometimes they are used and spoken of almost interchangeably, but most of the times they are not and they have to be identified. It's possible to convert from one format to another using **OpenSSL x509** command.
### Certificate Fields
A certificate is composed of fields, and in version 3, a set of extensions:

- **Version** - there are three certificate verions - 1, 2, and 3, encoded as values 0, 1 and 2. Version 1 supports only basic fields; version 2 adds unique identifiers and version 3 adds extensions. Most of the certificates are in v3 format.
- **Serial Number** - are nonsequential data and contain at least 20 bits of entropy and were initially positive integers that identify a certificate issued by a given CA.
- **Signature Algorithm** - This field specifies the algorithm for the certificate signature. It's placed inside the certificate so it can be protected by the signature.
- **Issuer** - This field contains the **distinguished name** (**DN**) of the certificate issuer.
- **Validity** - Contains the validity period during which the certificate is valid.
- **Subject** - Is the **Distinguished name** of the entity associated with the public key for which the certificate was issued to. Self-signed certificates have the same *DN* in their **Subject** and **Issuer** fields. The Subject field today is deprecated in favor of the *Subject Alternative Name* extension.
- **Public Key** This field contains the public key, represented by the *Subject Public-Key Info* structure (essentially an algorithm ID, optional parameters and then the public key itself). Public-key algorithms are specified in the **RFC 3279**.
### Certificate Extensions
The certificate extensions were introduced in v3 in order to add more flexibility to the certificate format. Each extension consists of a unique **object identifier** (**OID**), criticality indicator and value, which is basically an ASN.1 structure.

An extension marked as critical must be understood and successfully processed, otherwise the entire certificate must be rejected.

These are the extensions introduced:

- **Subject Alternative name** - This extension replaces the *Subject* field and supports bindings to multiple identities specified by a DNS name, IP address or URI, differently from the *Subject* field which usually is based on the CN component, supporting only hostnames and does not specify how multiple identities are handled.
- **Name Constraints** - Can be used to constrain identities for which a CA can issue certificates to. ID namespaces can be excluded or permitted. This is useful in instances where an organization can be allowed to obtain a subordinate CA that can issue certificates only for the company-owned domain names. This way, the CA cannot issue certificates for arbitrary sites, posing danger to the environment.
- **Basic Constraints** - Is used to indicate a CA certificate and control the depth of the subordinate through the *path length constraint* field, .e.g, whether the CA certificate can issue further nested CA certificates and how deep. In theory, CA certificates must include this extension, but in practice some root certificates issued as version 1 are still used despite the fact that they contain no extensions.
- **Key Usage** - Defines the possible uses of the key contained in the certificate. There is a fixed number of uses that can be set on a particular certificate, .e.g, A CAcert could have the *Certificate Signer* and *CRL Signer* bits set.
- **Extended Key Usage** (**EKU**) - Allows arbitrary additional purposes to be specified, giving more flexibility in either determining or restricting the public key usage by their OIDs. E.g., end-entity certificates usually carry the *id-kp-serverAuth* and *id-kp-clientAuth* OIDs. Code signing certificates use the *id-kp-codeSigning* OID, etc.

*Note That: Although* **RFC 5280** *indicates that the Extended Key Usage should only be used on end-entity certificates. However, in pratice, this extension is used on intermediate CAcerts to constrain the usage of the certificates issued from them. Baseline Requirements in particular require the use of EKU constraints for an intermediate certificate to be consudered technically constrained using name constraints*.

- **Certificate Policies** - Contains a list of one or more policies, which consists of an OID and an optional qualifier. When present, the qualifier usually contains the URI at which the full text of the policy can be obtained. As per the *Baseline Requirements*, an end-entity certificate must always include at least one policy to indicate the terms under which the certificate was issued. It can also be used to indicate cert validation type.
- **CRL Distribution Points** - Is used to determine the location of the *Certificate Revocation List* (**CRL**) information, usually provided as an LDAP or HTTP URI. Generally, a certificate must provide either a CRL or OCSP revocation information.
- **Authority Information Access** - Indicates how to access certain additional information and services provided by the issuing CA, e.g., an OSCP responder, provided as an HTTP URI. With that, the responder could be used to check for revocation information, and some certificates also include the URI where the issuing certificate can be found - this proves useful for reconstruction of an incomplete certificate chain.
- **Subject Key Identifier** - Contains a unique value used to identify certificates that contain a particular public key. It's recommended that the identifier be constructed from the public key itself (.e.g, by hashing). All CAcerts must include this extension and use the same identifier in the *Authority Key Identifier* extension of all issued certificates.
- **Authority Key Identifier** - Identifies the key that signed the certificate. It can be used during the certificate path building to identify the parent certificate.

*NOTE that the* **RFC5280** *defines other extensions that are rarely used, .e.g, Delta CRL, Distribution Poin, Inhibit anyPolicy, Issuer Alternative Name, Policy Constraints, Policy Mappings, Subject Directory Attributes, and Subject Information Access.

### Certificate Lifecycle
The Certificate lifecycle begins when a subscriber prepares a **Certificate Signing Request** (**CSR**) and submits it to the CA of their choice. The purpose of the CSR is to carry the relevant public key as well as to demonstrate ownership of the corresponding private key (using a signature). Often, CAs will override the CSR metadata values and use oher sources for information they embed in the certificates.

The CA then continues the validation procedure, using different steps depending on the certificate requested:
- **Domain Validation** (**DV**) - are issued based on proof of control over a domain name. Mostly, that means sending a confirmation email to one of the approved email addresses. In case the recipient approves (e.g., follows the link in the email), then the certificate is issued.
- **Organization Validation** (**OV**) - require identity and authenticity verification.
- **Extended Validation** (**EV**) - introduced to address the lack of consistency in OV certificates.

After the validation is successful, the CA issues the certificate. In addition to the certificate itself, the CA will also provide all the intermediary certificates required to chain to their root.



