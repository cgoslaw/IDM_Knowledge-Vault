# Kerberos Authentication

## Kerberos Components
Kerberos is a centralized authentication system, one of the most successful protocols to have emerged. At version 5, it provides benefits such as **SSO (Single Sign-on)**, no passwords sent over communication channels, mutual authentication and allowing authentication between unrelated entities by operating as a trusted third party. The Kerberos v5 protocol is defined in the **RFC4120**.

There are some terms related to the Kerberos authentication process:

**AS** - Authentication Service, which validades that a user is present in the Kerberos DB.

**TGS** - Ticket Granting Service, which provides an authenticated user with a Ticket Granting Ticket.

**TGT** - Ticket Granting Ticket, an encrypted authentication file with a limited period, used to obtain session tickets.

**Session Ticket** - A small encrypted authentication file with a limited period, used to encrypt communication between two endpoints

**KDC** - Key Distribution Center, which is the Kerberos server running the **AS** and **TGS** services.

**principal** - An identity that can be assigned a ticket. A principal has the following format: primary[/instance]@REALM.

**ticket** - A piece of encrypted data representing an authenticated state, also known as a credential. A ticket may be a Ticket Granting Ticket or a session ticket.

**symmetric key** - A key used to encrypt and decrypt a payload

**AES** - The Advanced Encryption Standard protocol replaced DES as the preferred symmetric key protocol.

**asymmetric keys** - A pair of keys; one used to encrypt and another to decrypt.

**keytab** - A file containing Kerberos credentials

**Credentials Cache** - A credential cache, or **ccache** stores Kerberos credentials while they remain valid and while the user's session lasts. This allows to authenticate against a service multiple times without contacting the KDC everytime.

## Authentication Server (AS)
The **Authentication Server** (**AS**) is the Kerberos server that validades the identity of an entity present in the Kerberos database and grants **Ticket-Granting Tickets**. The Authentication Server keeps and maintains account information of all the security principals in the realm and along with this information, it also maintains cryptographic keys that are either derived from a user's login password, or in case of a service/host, are dinamically generated. Each one of these cryptographic keys is a shared secret between the KDC and that one principal.

## Ticket Granting Server (TGS)
The **Ticket Granting Service** (**TGS**) is the server that confirms that a known user, previously validated by the *Authentication Server*, is making an access request to a known service and grants **Service Tickets**. The **TGS** also maintains a cache (the **TGS Cache**) of recently received *user authenticators*, so it can be checked to ensure that an Authenticator just received is not already in it, providing replay protection.

## Kerberos Principals
A Kerberos principal is a unique identity to which the KDC can assign tickets. Principals can have multiple number of components and each component is separated by a separator, generally **/**. The last component is always the realm, separated from the rest of the principal by the realm separator, generally the **@**. If there's no realm component in the principal, then it will be assumed that the principal is in the default realm for the context in which it is being used.

A typical Kerberos V5 principal is divided into three parts: The primary, the instance and the realm: **primary/instance@REALM**.

- The primary is the first part of the principal. If the principal is an user then it's an username. In case of a host or service, then the hostname or service.
- The instance is an optional string that qualifies the primary. The instance is separated by a slash (/). So in case of a user, the instance will be null, although a user might have an additional principal, with an admin instance, used to adminstrate a service. Considering the user **sisyphus**, the user principal is **sisyphus@DOMAIN.COM**; the principal **sisyphus/admin@DOMAIN.COM**, however, is a completely different principal with separate credentials and permissions.

In the case of a host, the instance is the fully qualified hostname. So a service credential in a host can be, for example: **HTTP/webserver.domain.com@DOMAIN.COM**. 

- Lastly, the realm is the Kerberos realm. In almost all the cases the Kerberos realm is the domain name, but in upper-case letters. 
## Kerberos Keytabs
A keytab file is used to store long-term keys for principals in the local system. Usually, keytabs are used to allow server applications to accept authentications from clients but can also be used to obtain initial credentials for client applications. The keytabs are named using the format *type:value*, type usually being *FILE* and the value the absolute path of the file. A *MEMORY* type also can exist, which indicates a temporary keytab stored in the memory of the current process.

There are usually multiple entries in a keytab, where each of these entries consists of a timestamp (denoting when the entry was written to the keytab), the principal name, the key version number, the encryption type and the encryption key itself. There are usually multiple keys for encryption types as well as old version keys that can also be kept.
Users, differently from service and host principals, do not need keytabs for authentication since these they are able to type in their passwords upon prompt, or authenticate in another way such as smartcards.

They keytabs can be managed by the following commands:
- **klist** with the option *-k* displays the default keytab, or a defined keytab if followed by another keytab file. 
- **kadmin** with the *ktadd* option to create a new keytab by extracting keys from the KDC database.
- **ktutil** and **k5srvutil** which are used to manipulate the keytabs.

## Kerberos Messages
During the Kerberos authentication process, many messages are passed back and forth between the client, the **AS**, the **TGS** and the service the client requested access to. During every step, at least two messages are sent, some in plain text while others encrypted with a symmetric key. Among these messages, there are two important types:

- **Authenticators** - A record containg information that have been previously generated and encrypted with a **session key** known to the client. These messages allow the users to authenticate to the service and the service to the user, creating a mutual authentication.

- **Tickets** - A Ticket contain (most of) the information that needs to be passed along the authentication process: the client's identity, Service Name/ID, session keys, timestamps, time to live (ttl), etc., and they are encrypted using the server's secret key: In the case of a **Ticket Granting Ticket**, it's encrypted by the **Authentication Server** using the **TGS Secret Key**, while a **Service Ticket** is encrypted by the **Ticket Granting Server** using the **Service's Service Secret Key**.

## Kerberos Tickets
In Kerberos, a ticket is a record that helps a client authenticate itself to a server or service. It contains some data needed for identity validation such as *client's identity*, a *session key*, a *timestamp* and other information. This information is always encrypted using the server's secret key.

There are two types of Tickets that are used throughout the Kerberos authentication process:

- The **Ticket Granting Ticket** is granted by the **Authentication Server** and encrypted with the **TGS Secret Key**. The ticket is sent over to the client during the first response for a request. The client will send the **TGT** over to the **Ticket Granting Server** to be decrypted and "exchanged" by a **Service Ticket**.
A Ticket Granting Ticket contains the following data to be validated by the **TGS**:
- The **User Name/ID**,
- The **TGS Name/ID**,
- The message's **timestamp**,
- The user's **IP address** (not always required),
- The **lifetime** for the **TGT**,
- The **TGS Session Key**

- The **Service Ticket** is granted by the **Ticket Granting Server** and encrypted by the **Service's Service Secret Key**. The *service ticket* is sent to the client after the **TGT** validation by the **TGS** is successfully passed, and it will be used to present the required data to the Service so it can later perform a last validation.
A Service Ticket contains data similar to what a **TGT** has:
- The **User Name/ID**,
- The **Service Name/ID**,
- The message's **timestamp**,
- The user's **IP address** (not always required),
- The **lifetime** for the **Service Ticket**,
- The **Service Session Key**
### Ticket Flags
TODO

## Session and Sub-session keys
A Session key is a temporary encryption key used between two principals, with a lifetime limited to the duration of a single login *session*. The session key is generated by the KDC.

A Sub-session key, or *subkey* is also a temporay encryption key used between two principals. They are selected and exchanged by the principals using the session key, and with a lifetime limited to the duration of a single association.
# The Kerberos Authentication
The heart of the Kerberos authentication is the **Key Distribution Centre** (**KDC**). It supplies tickets and generate temporary session keys that allow clients to access services and servers. It stores secret symmetric keys for users and services.

The Authentication process occurs in these steps:
1. Initially, the client sends an unencrypted request to a certain service/server and is first sent to the **Authentication Server**. This request contains the following attributes:
```
     Plaintext MSG
+----------------------+
|      Attributes      |
|----------------------|
| * User Name/ID       |
| * Service Name/ID    |
| * User IP Address    |
| * Requested lifetime |
|   for the TGT        |
+----------------------+
```
2. When the **Authentication Server** (**AS**) receives the request with the message attributes sent by the client, it first looks for the *User Name/ID* and validates it against the **Kerberos Database** if the user is present in the list of users in the KDC. If the user is present, the **AS** prepares two messages to send back to the client:
```
+====================================+
| Encrypted with Client's Secret Key |
|====================================+
|             Attributes             |
+------------------------------------+
|           * TGS Name/ID            |
|           * Message Timestamp      |
|           * Lifetime               |
|           * TGS Session Key        |
+------------------------------------+

      ## Ticket Granting Ticket ##
+====================================+
|   Encrypted with TGS Secret Key    |
|====================================+
|             Attributes             |
+------------------------------------+
|           * User Name/ID           |
|           * TGS Name/ID            |
|           * Message Timestamp      |
|           * User IP Address        |
|           * Lifetime for the TGT   |
|           * TGS Session Key        |
+------------------------------------+
```
3. When the user receives back the two messages, it usually comes by the form of the password prompt. When the user inputs the password, Kerberos then add a salt to the password, which is usually the user's own username at realm name (user@realm.com) and a **key version number** (**kvno**), which is useful when long lib keys are used. The user salted password is run through a hashing algorithm to generate the *User's Secret Key*, and this key is used to decrypt the first message which was previously encrypted with the user's key.

**NOTE** that the user cannot decrypt the **Ticket Granting Ticket** because the user doesn't have the TGS's secret key.

When the first message is decrypted by inputting the correct password, the client get access to the **TGS Name/ID** and the **TGS Session Key**. The client then creates two new messages and send them along with the **TGT** to the **Ticket Granting Server**:
```
  ## Plaintext MSG ##
+----------------------+
|      Attributes      |
|----------------------|
| * Service Name/ID    |
| * Requested lifetime |
|   for the TGT        |
+----------------------+

        ## User Authenticator ##
+====================================+
|   Encrypted with TGS Session Key   |
|====================================+
|             Attributes             |
+------------------------------------+
|           * User Name/ID           |
|           * Message Timestamp      |
+------------------------------------+
```
4. When the **TGS** receives the message from the client, it first looks for the *Service Name/ID* contained in the plaintext message and verifies if the Service's ID is in the list of known services in the **Kerberos Database**. If the service is in the list, the **TGS** decrypts the **Ticket-Granting Ticket** with its secret key.

When the **TGT** is decrypted, the *TGS* uses the contained **TGS Session Key** to decrypt the **User Authenticator** message and then starts validating all the data contained in the messages sent over by the client:
- The TGS verifies if the *User Name/ID* in both the **TGT** and the **User Authenticator** match.
- Then it compares the *timestamp*, which is usually configured to tolerate up to two minute difference between them.
- It compares the *IP address* in the TGT to the IP address of the user it received the messages from.
- Checks if the TGT has not expired.
- Verifies if the Authenticator is already present in the **ccache**. If it's not in the cache, it will be added.

If all the verifications are successfully passed, the **Ticket Granting Service** creates a message to send back to the user along with a new **Service Ticket** *"exchanged"* by the returning **Ticket Granting Ticket**. The **TGS** also generates a random symmetric session key and add them to both message blocks:
```
+====================================+
|   Encrypted with TGS Session Key   |
|====================================+
|             Attributes             |
+------------------------------------+
|           * Service Name/ID        |
|           * Message Timestamp      |
|           * Lifetime               |
|           * Service Session Key    |
+------------------------------------+

         ## Service Ticket ##
+====================================+
|          Encrypted with the        |
|      Service's Service Secret Key  |
|====================================+
|             Attributes             |
+------------------------------------+
|           * User Name/ID           |
|           * Service Name/ID        |
|           * Message Timestamp      |
|           * User IP Address        |
|           * Lifetime for the       |
|             Service Ticket         |
|           * Service Session Key    |
+------------------------------------+
```
5. When the user receives the message block sent by the **TGS**, the client decrypts it with the **TGS Session Key** received during the step no. 3, and gets access to the **Service Session Key**. The user then proceeds to creating a new **User Authenticator** and encrypts it with the *Service Session Key*.

**NOTE** that the **Service Ticket** cannot be decrypted by the user since it was encrypted with the **Service's Service Secret Key**.

6. When the service receives the messages from the user, it decrypts the **Service Ticket** using its own **Service Secret Key** contained in a **keytab** located in a directory it has access to, gaining access to the *Service Session Key* which is used to decrypt the **User Authenticator**. The service then validates the data containted in both messages by performing the same validation steps the **Ticket Granting Service** performs.

If all the validation passes, the Service also checks its **Service Cache** for the **User Authenticator**, and if it's not in the cache, it will be added.

The service proceeds to respond to the client with a **Service Authenticator**, similar to a *User Authenticator*, but containing the *Service Name/ID* instead of the *User Name/ID*, and encrypts it with the **Service Session Key** contained in the **Service Ticket**.

7. Lastly, the client receives the message from the service and decrypts it using the **Service Session Key** previously received from the **Ticket Granting Server** and validates if the *Service Name/ID* is the same it wants access to and checks the timestamp. The client will also check in its own cache and it will cache the Service Ticket in case it was not cached before.

This concludes the Kerberos authentication and the client gains access to the Kerberized service it wanted to access.
