# Identity Management Services
Red Hat Identity management provides a centralized way to manage identity stores, authentication, policies and authorization policies in a Linux-based domain, reducing the administrative overhead of managingdifferent services and hosts individually.

A number of different services are installed together with the IdM server, including the Directory Server, a Certificate Authority (CA), DNS, Kerberos, and some others.
```
+-----------------------------------------------------------------+
|   Red Hat Identity Management Server                            |
|                                                                 |
|--------------------+                                            |
| Certificate System | << ------+                                 |
|--------------------+          |                                 |
|--------------------+          |           +------------------+  |
|     Kerberos KDC   | << ------+------- >> | Directory Server |  |
|--------------------+          |           +------------------+  |
|--------------------+          |                                 |
|     DNS Server     | << ------+                                 |
|--------------------+                                            |
|--------------------+                                            |
|     NTP Server     |                                            |
|--------------------+                                            |
|                                                                 |
|   +-------------------------------------------------------+     |
|   |                 Management Interface                  |     |
|   +-------------------------------------------------------+     |
|             |                               |                   |
|             |                               |                   |
|             |                               |                   |
|      +------------+                   +-----------+             |
|      |   Web UI   |                   | CLI Tools |             |
|      +------------+                   +-----------+             |
+-----------------------------------------------------------------+
```
## Managing the IdM Server from the Command Line
Although the IdM server provides a Management Interface GUI, there are many command-line tools to manage the server. Among the tools, there are:
- Kerberos tools, among them **kinit**, **klist** and **kdestroy**, that are used for obtaining and caching kerberos tickets, listing and destroying them respectively. Kinit is used to log into the IdM from the command line.
- **ipactl**, a control interface used to control the IPA environment. It's used to stop, start or restart the IdM server along with its associated services.
- **ipa**, a control interface used to execute administrative commands specified by the **COMMAND** argument, e.g., user-add, group-add, etc. The majority of the commands are executed remotely over XML-RPC on a IPA server listed in the **/etc/ipa/default.conf** configuration file.

## Managing Password Policies
There are many options applicable to password policies, among them:
- maxlife: the maximum password lifetime in days
- minlfie: the minimum password lifetime in days
- history: the number of previous passwords that IdM will remember
- minclasses: the minimum number of character classes that a password must contain, such as uppercase, lowercase, numbers, and special characters
- minlength: the minimum length of a password

Setting a password policy attribute to 0 means disabling the attribute restriction e.g., if maximum lifetime is set to 0, the password will never expire.
### Global and Group-specific Password Policies
Upon the installation, IdM automatically creates a global password policy, which is the default password policy. It can be configured with the policy attributes and it will be applied to all users by default in case there are no other group password policies.

These groups can be configured with attributes such as priority.
### Password Policy Priorities
Only one password policy can be in effect at a time for a user. If a user has multiple group password policies assigned, the one with the lowest priority value takes precedence and all rules defined in other policies are ignored.

The lowest supported priority value is 0.

The global password policy does not have a priority value set. It servers as a fallback policy when no group password policy is set for a user. The global password policy can never take precedence over a group password policy.
## Managing Users and User Life Cycle
The user life cycle management starts when a user account is provisioned. A provisioned account begins the relationship that continues through all changes, such as promotions, location or status changes. When a user leaves the organization, the account should be deprovisioned.
### User Account States
IdM supports three user account states:

**Stage**

An initial state in which a user is not permitted to authenticate to the domain. This state is useful when an account is being provisioned for a user who has yet to be part of the organization. Some of the user account properties required for active users might not be set.

**Active**

Active users are the default state in the domain. They are allowed to authenticate and all required account properties must be set.

**Preserved**

Preserved users are formerly active users. They are considered inactive and cannot authenticate to the domain. They retain most of the account properties they had as active users, but they are removed of all user groups.

User entries can also be permanently deleted from the IdM database. Deleting a user entry permanently removes the entry itself and all its information, including group membership and password history. External configuration to clients such as system account and home directory is not deleted, but it's no longer accessible through the domain. When deleted, a user cannot be restored.
# Managing IdM Clients
## Prerequisites for installing a Client
Some prerequisites must be checked before enrolling a client to an IdM server:
- The *ipa-client* package must be installed on the client
- The system must be able to reach the Kerberos domain. This means that there must be an available kerberos identity like an administrative user, or by adding the machine directly to the **KDC (Key Distribution Center)** on the IdM server.
- If there's an Active Directory Domain Controller on the same network that also serves DNS records, the AD could prevent the client from automatically detecting the IdM server due to the ldap SRV records, that would also be pointing to the Active Directory. If that's the case, the IdM server must be passed directly to the **ipa-client-install** command
- The NSCD (Name Service Caching Daemon) should be disabled due to incompatibilities with SSSD due to both services providing caching. If the NSCD cannot be disabled, NSCD should be configured to only cache maps that SSSD do not cache. 
## Credential requirements for Installing Clients
During the installation and integration with the IdM server, a set of credentials with enough permissions should be passed so the client could be enrolled. For this, three methods can be used:
1. The first and default method is to provide the credentials of an authorized user. This user will provide the credentials and the machine will be enrolled to the IdM server.
2. With an *OTP (One-Time Password)*, that can be generated by the IdM server. In order to use the OTP method, it's necessary to use the --random option when running the ipa-client-install command.
3. With a principal from a previous enrollment. This method will require an existing keytab that will be passed with the *--keytab* option.
## Installation Methods
After choosing a way to pass the credentials, one can choose the method to perform the installation:
1. Manually and interactively. This means that the installation will prompt for user input to configure various settings.
2. The automated and non-interactive method. This will require that a set of options be passed along with the **--unattended** option, so the command is able to run without being prompted.
# IdM Client Components
IdM clients do not require dedicated client software to be part of a domain, requiring only the configuration of services and libraries such as Kerberos and DNS.
Some services, however, greatly ease the management of these clients, and they include:
- **The System Security Services Daemon** (**SSSD**), a client-side application that provides caching for credentials and integration management. It also provides offline client authentication (credential caching) and integration with other services like sudo and **Host-Based Access Control** (**HBAC**).
- **The certmonger** service, that monitors and renews security certificates on the client. It also requests new certificates for services on the system.
## Client Log Files
In a client, there are several log files that can be used to analyse and troubleshoot issues:
```
+==================================+========================================================================+
| 	Directory or File          |	                          Description                               |
|===========================================================================================================|
| /var/log/ipaserver-install.log   | The Installation log for the IdM server                                |
|----------------------------------|------------------------------------------------------------------------|
| /var/log/ipareplica-install.log  | The installation log for an IdM replica                                |
|----------------------------------|------------------------------------------------------------------------|
| /var/log/ipaclient-install.log   | The Installation log for the IdM client                                |
|----------------------------------|------------------------------------------------------------------------|
| /var/log/sssd/		   | SSSD log files		                                            |
|----------------------------------|------------------------------------------------------------------------|
| ~/.ipa/log/cli.log               | The log file for errors return by XML-RPC calls and                    |
|				   | responses by the ipa utility.                  			    |
|----------------------------------|------------------------------------------------------------------------| 
| /etc/logrotate.d/	           | The log rotation policies for DNS, SSSD, Kerberos, Apache, etc.        |
|----------------------------------|------------------------------------------------------------------------|
| /var/log/httpd/		   | Log files for the Apache server		            	            |
|----------------------------------|------------------------------------------------------------------------|
|                 		   | Log file that contains DNS error messages along with other system      |
| /var/log/messages                | messages. Note that DNS logging to this file is not enabled by default.|
|                                  | The command **rndc querylog** must be run to enable it.		    |
|----------------------------------|------------------------------------------------------------------------|
| /var/log/secure		   | The log file that contains details about authentication process        |
|                                  | and pam that can be helpful when an authentication issue is happening. |
-----------------------------------|------------------------------------------------------------------------|
| /var/log/krb5kdc.log		   | The file with log entries related to the enrollment of clients         |
|                                  | on the IdM server                                                      |
+----------------------------------+------------------------------------------------------------------------+
```


