# The IdM Certificate Authority
During the installation of an IdM server, it's possible to choose to use an external CA or the integrated CA provided by the service. The integrated CA is a component an upstream project - the Dogtag Certificate System. The CA is a web app running in a Tomcat container, and the service is the **pki-tomcatd@pki-tomcat.service**.

Also, there's the **Key Recovery Authority** (**KRA**) component from Dogtag that's also used in IdM. The **KRA** provides the **IdM Vault**.

## Certmonger
The **certmonger** service is included to automate the burden of certificate administration tasks, by tracking certificates and warning when their expiry date is close, being able to attempt to renew it in case the private key location is known to the service.

The service is integrated with IdM and is invoked by using the **getcert** or **ipa-getcert** commands.

**Certmonger** consists of two main components:
- The **certmonger** daemon, which is the engine tracking the list of certificates and lauching renewal commands
- The **getcert** utility for the CLI, which allows the sysadmin to send commands to the certmonger daemon. More specifically, the utility can be used to request a new certificate, view the list of certificates that certmonger is currenctly tacking, start/stop tracking a certificate and renew a certificate.

By default, **certmonger** can automatically obtain three kinds of certificates that differ in what authority source the certificate employs:
- *Self-signed certificate*
- *A certificate from the Dogtag Certificate System CA as part of Red Hat Enterprise Linux IdM*
- *Certificate signed by a local CA present on the system*

## Secrets & Vaults
In IdM, a **secret** is any piece of information that can be made available only to certain people. This information can be used for authentication, which might then include PINs, certificates, password or keys. For storing these secrets, **vaults** can be used - A **vault** can be considered like a password manager and can contain one secret, but a there can be several vaults inside a vault container which are dynamically created or deleted. These vaults containing secrets can be shared with users or services.

Vaults can be enabled by installing the **Key Recovery Authority** (**KRA**) Certificate System (CS) on an specific idM server. There are some prerequisites such as:
- The user must be logged as root on the IdM server,
- An IdM CA must be installed on the server,
- The user must have the Directory Manager credentials.

### Vault Permissions
There are three permissions that can be assigned in a vault:
- **Owner**: Owners are users or services and they can modify the vault's properties. There must be at least one owner for a vault.
- **Administrator**: Administrators can manage all vaults
- **Member**: A general user that can access a vault created by an owner.

### Vault Types
- **Standard**: Allows the owner and members to retrieve secrets without providing passwords.
- **Symmetric**: Protected by a symmetric key, the key must be provided when archiving and retrieving secrets.
- **Asymmetric**: Proteced with an assymetric key-pair. Secrets are archived using the public key and retrieved using the private key.

### Vault & Vault Container Ownership types
Vaults and Vault containers can be classified by three types:
- **User vault**: Which is a private vault for a user. The owner will always be a single user and any user can own one or more user vaults if allowed by the IdM admin.
- **Service vault**: A private vault for a service. Similar to a *user vault*.
- **Shared Vault**: Shared by multiple users and services. These are shared by multiple users and/or services as well as by the administrator who created the vault.


