# Kerberized HTTPD Configuration for RHEL 7.9

1. Add a new HTTP service for a host, either by CLI or by the Web UI.
2. Enable the necessary ports for the webserver.
3. Retrieve the service kerberos keytab.
4. Ensure the service user can read the keytab.
5. Configure the Apache HTTPD service to allow Kerberos authentication
6. Verify if the restricted page can be accessed.
