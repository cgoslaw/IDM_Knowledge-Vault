# Installing MariaDB in a Red Hat IDM domain / OpenSSL

1. Generate a certificate for an user that will manage the database.
2. Create a pkcs12 file with the key and the certificates.
3. Create a vault for storing the user pkcs#12 files.
4. Generate a service certificate for the mariadb instance.
5. Install and configure mariadb-server for SSL/TLS access.
6. Set up user authentication with certificates.
