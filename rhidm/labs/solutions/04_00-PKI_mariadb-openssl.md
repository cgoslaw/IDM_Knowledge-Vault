# Installing MariaDB in a Red Hat IDM domain / OpenSSL

1. Generate a certificate for an user that will manage the database.

To generate the user certificate, a *CSR* must be generated firstly. Using the openssl req utility that generates PKCS#10 requests and certificates, the **-keyout** and **-out** options will determine the output files for the private key and csr files respectively, while the **-subj** option defines the subject without being prompted by the utility.
```
# openssl req -newkey rsa:4096 -keyout ipauser01.key -out ipauser01.csr -subj "/O=UNIX.DOMAIN.NET/CN=ipauser01"
Generating a RSA private key
......................++++
..............................++++
writing new private key to 'ipauser01.key'
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
```
After generating the CSR, it must be signed by the CA. In this case, the CA is the integrated CA in the IdM installation, so the IdM server will sign the certificate request and generate the user certificate.
```
# ipa cert-request --profile-id IECUserRoles --certificate-out ~/ipauser01-cert/ipauser01.pem --principal ipauser01@UNIX.DOMAIN.NET ~/ipauser01-cert/ipauser01.csr
  Issuing CA: ipa
  Certificate: <output ommited>
..
..
  Subject: CN=ipauser01,O=UNIX.DOMAIN.NET
  Issuer: CN=Certificate Authority,O=UNIX.DOMAIN.NET
  Not Before: Sun Mar 06 01:32:44 2022 UTC
  Not After: Wed Mar 06 01:32:44 2024 UTC
  Serial number: 17
  Serial number (hex): 0x11
```
It can be verified if the certificate has been correctly generated to the user by using the **ipa user-show** command:
```
# ipa user-show ipauser01
  User login: ipauser01
  First name: IPA
  Last name: Ipauser
  Home directory: /home/ipauser01
  Login shell: /bin/bash
  Principal name: ipauser01@UNIX.DOMAIN.NET
  Principal alias: ipauser01@UNIX.DOMAIN.NET
  Email address: ipauser01@unix.domain.net
  UID: 2000000009
  GID: 2000000009
  Certificate: <output ommited>
  ..
  ..
  ..
  Account disabled: False
  Password: True
  Member of groups: admins, group01, ipausers
  Kerberos keys available: True
```
2. Create a pkcs12 file with the key and the certificates.
In order for the user to have an easier time managing his certificates, both the certificate and the key will be stored into a single PKCS#12 certificate file.  This file can then be managed and if needed, copied over hosts and then be either used as it is on utilities that support it, or the key along with the x509 certificate can be retrieved from the PKCS#12 file to be used separately.
The openssl pkcs12 utility will be used with the **-export** option, meaning that a pkcs#12 file will be created instead of parsed; **-inkey** and **-in** to specify which key and certificate files respectively, while **-name** will give it a "friendly name" which is displayed in list boxes by some softwares and lastly **-out** to specify the newly generated pkcs#12 file path.
```
# openssl pkcs12 -export -inkey ipauser01.key -in ipauser01.pem -name ipauser01 -out ipauser01.p12
Enter pass phrase for ipauser01.key:
Enter Export Password:
Verifying - Enter Export Password:
```
3. Create a vault for storing the user pkcs#12 files.
In IdM, the user has the possibility of creating a vault for storing secrets and be able to retrieve it from any machine in the domain without having to copy it manually over hosts. For that, the user must authenticate to the KDC with **kinit** and then he will be able to create the vault with **ipa vault-add** use both **ipa vault-archive** and **ipa vault-retrieve** for storing and retrieving the files.
```
# kinit -V ipauser01
Authenticated to Kerberos v5

# ipa vault-add mariadb-vault --type symmetric
New password:
Enter New password again to verify:
---------------------------
Added vault "mariadb-vault"
---------------------------
  Vault name: mariadb-vault
  Type: symmetric
  Salt: nk0vxHTvF0u0oMlrgdCoiQ==
  Owner users: ipauser01
  Vault user: ipauser01

# ipa vault-archive mariadb-vault --in ipauser01.p12
Password:
----------------------------------------
Archived data into vault "mariadb-vault"
----------------------------------------
```
4. Generate a service certificate for the mariadb instance.
For authentication based on TLS/SSL, a certificate must be generated for the mariadb instance that will be installed soon in a domain client machine. For that a service must be first added.
```
# ipa service-add mysql/worker05.unix.domain.net
----------------------------------------------------------------
Added service "mysql/worker05.unix.domain.net@UNIX.DOMAIN.NET"
----------------------------------------------------------------
  Principal name: mysql/worker05.unix.domain.net@UNIX.DOMAIN.NET
  Principal alias: mysql/worker05.unix.domain.net@UNIX.DOMAIN.NET
  Managed by: worker05.unix.domain.net
```
The directory where the certificates will be stored then must be set up for permissions so the service can access and read the files, and the SELinux context must also be appropriately configured so certmonger is able to read it:
```
# mkdir /etc/db-certs && chown mysql: /etc/db-certs
# semanage fcontext -a -t cert_t "/etc/db-certs(/.*)?"

# restorecon -R -v /etc/db-certs/
restorecon reset /etc/db-certs context unconfined_u:object_r:etc_t:s0->unconfined_u:object_r:cert_t:s0
```
After the service was added to the IdM domain, its certificate can be exported to the client. The IPA's CACERT file must also be copied over for mariadb to work with TLS. The certificate can be exported with the **ipa-getcert request** command.
```
ipa-getcert request -f /etc/db-certs/cert.pem -k /etc/db-certs/key.pem -K mysql/worker05.unix.domain.net -D worker05.unix.domain.net
New signing request "20220306020528" added.

# getcert list -i 20220306020528
Number of certificates and requests being tracked: 1.
Request ID '20220306020528':
        status: MONITORING
        stuck: no
        key pair storage: type=FILE,location='/etc/db-certs/key.pem'
        certificate: type=FILE,location='/etc/db-certs/cert.pem'
        CA: IPA
        issuer: CN=Certificate Authority,O=UNIX.DOMAIN.NET
        subject: CN=worker05.unix.domain.net,O=UNIX.DOMAIN.NET
        expires: 2024-03-06 02:04:58 UTC
        dns: worker05.unix.domain.net
        principal name: mysql/worker05.unix.domain.net@UNIX.DOMAIN.NET
        key usage: digitalSignature,nonRepudiation,keyEncipherment,dataEncipherment
        eku: id-kp-serverAuth,id-kp-clientAuth
        pre-save command:
        post-save command:
        track: yes
        auto-renew: yes
```
Copying the CAcert over to the mariadb host:
```
# scp root@idm-server:/etc/ipa/ca.crt /etc/db-certs/
Password:
ca.crt                                                                     100% 1655     1.8MB/s   00:00
```
5. Install and configure mariadb-server.
After installing the mariadb-server package, the /etc/my.cnf.d/server.cnf file must be configured for ssl options, under the *[mariadb]* section.
```
# yum install mariadb-server

# vim /etc/my.cnf.d/server.cnf
...
...
[mariadb]
ssl-ca=/etc/db-certs/ca.crt
ssl-cert=/etc/db-certs/cert.pem
ssl-key=/etc/db-certs/key.pem
ssl_cipher=TLSv1.2
```
After installing and configuring the ssl options, the service can be enabled:
```
# systemctl enable --now mariadb.service

# mysql_secure_installation
NOTE:.NETNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n]
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n]
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n]
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n]
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

 Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n]
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```
6. Set up user authentication with certificates.
After setting up the mariadb instance, the SSL variables can be verified if they have been set up correctly:
```
# mysql -u root -p

MariaDB [(none)]> show variables like '%ssl%';
+---------------+------------------------+
| Variable_name | Value                  |
+---------------+------------------------+
| have_openssl  | YES                    |
| have_ssl      | YES                    |
| ssl_ca        | /etc/db-certs/ca.crt   |
| ssl_capath    |                        |
| ssl_cert      | /etc/db-certs/cert.pem |
| ssl_cipher    | TLSv1.2                |
| ssl_key       | /etc/db-certs/key.pem  |
+---------------+------------------------+
7 rows in set (0.00 sec)
```
Set up a new database and privileges for the remote user, where the user can access the database only with one's certificates:

```
MariaDB [(none)]> create database main_database;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON main_database.* TO 'ipauser01'@'localhost' REQUIRE ISSUER '/O=UNIX.DOMAIN.NET/CN=Certificate Authority' AND SUBJECT '/O=UNIX.DOMAIN.NET/CN=ipauser01';
Query OK, 0 rows affected (0.00 sec)
```
The user's pkcs#12 certificate file should be retrieved from the vault:

```
# ipa vault-retrieve mariadb-vault --out ~/ipauser01.p12
Password:
-----------------------------------------
Retrieved data from vault "mariadb-vault"
-----------------------------------------
```
And both the key and pem files extracted with openssl so they can be used by the user to access the database:
```
# openssl pkcs12 -in ipauser01.p12 -nocerts -nodes -out ipauser01.key
Enter Import Password:
MAC verified OK

# openssl pkcs12 -in ipauser01.p12 -clcerts -nokeys -out ipauser01.pem
Enter Import Password:
MAC verified OK
```
Lastly, with all the certificate files in place, the user can authenticate to the database:
```
# mysql -u ipauser01 --ssl_cert ipauser01.pem --ssl_key ipauser01.key
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SELECT CURRENT_USER();
+---------------------+
|   CURRENT_USER()    |
+---------------------+
| ipauser01@localhost |
+---------------------+
1 row in set (0.00 sec)

MariaDB [(none)]> create table main_database.documents ( level INT NOT NULL, name VARCHAR(128) NOT NULL);
Query OK, 0 rows affected (0.12 sec)

MariaDB [(none)]> describe main_database.documents;
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| level | int(11)      | NO   |     | NULL    |       |
| name  | varchar(128) | NO   |     | NULL    |       |
+-------+--------------+------+-----+---------+-------+
2 rows in set (0.00 sec)

MariaDB [(none)]> quit
Bye
```
