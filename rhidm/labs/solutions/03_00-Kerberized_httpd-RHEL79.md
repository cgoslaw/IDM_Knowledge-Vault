1. Add the HTTP service for the host, either by CLI or by the Web UI:
```
# ipa service-add HTTP/webserver.unix.domain.net@UNIX.DOMAIN.NET
---------------------------------------------------------------
Added service "HTTP/webserver.unix.domain.net@UNIX.DOMAIN.NET"
---------------------------------------------------------------
  Principal name: HTTP/webserver.unix.domain.net@UNIX.DOMAIN.NET
  Principal alias: HTTP/webserver.unix.domain.net@UNIX.DOMAIN.NET
  Managed by: webserver.unix.domain.net
```
2. Enable the necessary service ports on the firewall on the webserver:
```
# firewall-cmd --add-service={http,https} --permanent && firewall-cmd --reload
```
3. Retrieve the keytab for the service principal and:
```
# ipa-getkeytab -s idm-srv00.unix.domain.net -p HTTP/webserver.unix.domain.net@UNIX.DOMAIN.NET -k /etc/httpd/http.keytab
```
4. Change the keytab ownership so the service can access it with the service user:
```
# chown apache /etc/httpd/http.keytab
```
5. Install the kerberos authentication module package for apache HTTP:
```
# yum install mod_auth_kerb
```
6. Create a configuration file for the restricted url:
```
# vim /etc/httpd/conf.d/restricted.conf
<Location /restricted>
  AuthType Kerberos
  AuthName UNIX.DOMAIN.NET
  KrbMethodNegotiate On
  KrbMethodK5Passwd Off
  Require user kronos@UNIX.DOMAIN.NET
  KrbServiceName HTTP
  Krb5KeyTab /etc/httpd/http.keytab
</Location>
```
7. Reload or restart the httpd service:
```
# systemctl restart httpd
```
8. On a client, authenticate as the allowed user:
```
# kinit -V kronos
Using default cache: persistent:0:0
Using principal: kronos@UNIX.DOMAIN.NET
Password for kronos@UNIX.DOMAIN.NET:
Authenticated to Kerberos v5
```
9. Verify if the user can access the service:
```
# curl --negotiate -u: http://webserver.unix.domain.net/restricted/index.html

* Server auth using GSS-Negotiate with user ''
> GET /restricted/index.html HTTP/1.1
...
...
< HTTP/1.1 200 OK
< Date: Sun, 13 Feb 2022 23:46:22 GMT
< Server: Apache/2.4.6 (Red Hat Enterprise Linux) mod_auth_kerb/5.4
...
...
< Accept-Ranges: bytes
< Content-Length: 24
< Content-Type: text/html; charset=UTF-8
<
Hello Restricted World!
* Closing connection 0
```
