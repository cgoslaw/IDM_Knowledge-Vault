# Installing Red Hat Identity Management Server on a RHEL8

1. Deploy a new RHEL8 instance and ensure FIPS is disabled:
```
# cat proc/sys/crypto/fips_enabled
0
```
2. Lock the release version to RHEL8.4 and update the system.
```
# subscription-manager release set=8.4
```
3. Enable the IdM server module and install the necessary packages.
```
# dnf module enable idm:DL1/{dns,adtrust}
# dnf module install idm:DL1/{dns,adtrust}
```
4. Add the necessary network rules to the firewalld and make sure it survives reboot.
```
# firewall-cmd --add-service={freeipa-ldap,freeipa-ldaps,dns} --permanent && firewall-cmd --reload
```
5. Change the hostname to reflect the domain to be created.
```
# hostnamectl set-hostname ipa-srv00.unix.domain.net
```
6. Install the ipa-server with the options:
- Unnatended,
- Setup DNS, with a dns forwarder
```
ipa-server-install -U --realm=UNIX.DOMAIN.NET --domain=unix.domain.net --ds-password='password123' --admin-password='password123' --setup-dns --forwarder=10.0.0.190
```

