# Installing Red Hat Identity Management Server on a RHEL8

1. Deploy a new RHEL8 instance and ensure FIPS is disabled
2. Lock the release version to RHEL8.4 and update the system.
3. Enable the IdM server module and install the necessary packages.
4. Add the necessary network rules to the firewalld and make sure it survives reboot.
5. Change the hostname to reflect the domain to be created.
6. Install the ipa-server with the options:
- Unnatended,
- Setup DNS, with a dns forwarder
