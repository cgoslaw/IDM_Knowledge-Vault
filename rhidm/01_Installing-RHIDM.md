# Red Hat Identity Management
Red Hat Identity Management (**IdM**) was designed into Red Hat Enterprise Linux since RHEL 6.2, to simplify identity management. It implements *identity and access management* (**IAM**) with central authentication management of identities and security mechanisms, fine-grained access control policies, integrated *Public Key Infrastructure* (**PKI**) services, *two-factor authentication* (**2FA**) support, cross-realm Kerberos trusts with Active Directory (**AD**), and a feature for direct client connections to Active Directory. Although it is not an administrative tool for Active Directory domains, it can also synchronize identities with AD. And depending on the features it implements and the relative environment, IdM can facilitate the creation of Linux domains, allowing users to access both Linux and Windows resources, as well as to allow Windows users to access Linux resources.

Red Hat IdM also combines **LDAP**, **Kerberos**, **DNS** and **PKI** protocols, and offers user interface for both web-based and command line administration. The primary use for IdM is to provide identity services to Linux clients using established and open protocols, allowing RHEL systems to serve as Linux domain controllers. IdM is the Red Hat tuned and supported release of the upstream open source **FreeIPA** project, built from components including **PAM**, **BIND DNS**, **MIT Kerberos**, and **Dogtag PKI**, integrated with the core **389 LDAP Directory Server**.

### 389 Directory Server (LDAP)
An Open Source LDAP directory server. It has a flexible schema that can be customized to define entries for users, machines, network entities, etc. It's used as the back end that stores data for many applications.

A directory server organizes stored information into hierarchical structure of a directory tree. The information structure consists of root entries (*suffixes*), intermediate or container entries (subtrees or branches), and leaf entries (actual data). 389 Directory Server trees can be complex, consisting of several branch points, or very simple (flat) with few branch points. IdM uses the 389 Directory Server as an LDAP back-end using a flat and simple directory tree to store information related to user accounts, groups, services, policies, DNS zone and host entries, etc.

### MIT Kerberos
Kerberos is an authentication protocol that uses symmetric key cryptograpy to generate tickets that authenticate users to network services. Kerberos-based authentication is safer then password-based authentication. Passwords are never sent over the network. The Kerberos server is installed on the IdM domain controller, and all Kerberos data is stored in the back-end Directory Server. Directory Server defines access controls for stored Kerberos data. Due to all Kerberos data being stored in IdM's Directory Server back-end, Kerberos server is managed through IdM tools.

### Network Time Protocol (NTP)
NTP is used to synchronize clocks between systems over a network. A central server acts as an authoritative clock, and clients referencing that NTP server synchronize their time to match it. Many services require time synchronization to function, like Kerberos' timestamped tickets to determine their validity. By default, the IdM server acts as the NTP server for the domain it hosts whether the server is acting the domain's NTP server or relying on an external server.

### Domain Name Service (DNS BIND)
DNS is required for the IdM domain to ensure the clients are able to name resolve and contact each other, as well as the IdM server over the network. Some services such as Kerberos also depend on host names to identify their principal identities. DNS maps and resolves host names from and to IP addresses. DNS is required to enroll new clients in the domain, locate the IdM servers and all services and clients within the domain. The installation creates DNS zone records that allow clients and servers to use the DNS service discovery to find the LDAP and Kerberos servers (the IdM servers providing these services) that are required for participation in the IdM domain. It's recommended to have the IdM server also manage the DNS domain.

### Dogtag Certificate System
Services and applications primarily use certificates for TLS authentication and secure communication. Dogtag is included in IdM as a certificate authority (**CA**). This CA issues certificates to resources within the IdM domain, such as replicas, hosts, services, users and applications. Dogtag can be used as a root CA or it can have its policies defined by an existing external root or intermediate CA.
The Dogtag Certificate System component also providades a Vault (Data Recovery Manager) subsystem - a secure location for storing, retrieving and sharing secrets, that are security-sensitive data that should only be accessible by a limited group of people. 
## Installing Red Hat Identity Management
### Hardware Recommendations
Before installing IdM, it's important to design and size one's own topology. The recommended pratice is to install multiple, replicated servers for performance, redundancy and balanced data distribution. IdM stores information in cache, so proper RAM sizing is very important. It's possible to follow these rules to determine how much RAM an IdM server needs:
- An IdM server with 10.000 users and 100 groups requires a minimum of 3GB RAM and 1GB of swap space.
- A server with more than 100.000 users and 50.000 groups requires a minimum of 16GB RAM and 4GB of swap space.

A basic user or host entry with a certificate takes around 10KB in size. For large deployments, consider increasing the available memory amount since much of the data is stored in the cache.
### System Requirements
IdM should always be installed on a clean system without any existing DNS, Kerberos or Directory Server instances. When first installed, the installer backs up some system files it overwrites, and is backed up in **/var/lib/ipa/sysrestore/** directory.

Another requirement is to disable the **Name Service Cache Daemon** (**NSCD**). The service was used in earlier identity management solutions and was replaced by SSSD in IdM, being now mutually exclusive. IdM uses SSSD to perform caching and having both services might cause issues.

The server also must have the DNS properly configured. It needs to resolve its name using both forward and reverse DNS queries, since most of the IdM functions depend on DNS records, such as LDAP directory services, Kerberos and Active Directory integration. The hostname must also be set to a <hostname>.<fqdn> notation, and the fqdn cannot resolve the loopback address, but the server public address.

IdM uses a number of ports for communication with its services. The following ports must not be blocked by the firewall: 80, 443, 389, 636, 88, 464 and 53 - TCP; and 88, 464, 53 and 123 - UDP. All the ports are present in its firewalld service definition. So in order to easily open all the ports, the service **freeipa-ldap** for LDAP, **freeipa-ldaps** for LDAPs, or both. 

IPv6 must also be enabled at the kernel level. Although IPv6 address may not be used, IdM requires that the IPv6 stack is enabled. Otherwise, IdM will not be able to operate some plug-ins in the LDAP server, such as plug-ins necessary for interoperability with Active Directory.

### IdM Server Installation
IdM is included in the standard Red Hat Enterprise Linux subscription. To install the necessary packages, a yum install will suffice in a RHEL 7, and the module idm:DL1 must be enabled and installed for RHEL 8 systems.

#### IdM Install Script
Once the packages are installed, the IdM instance can be created with its install script: **ipa-server-install** command. Running with no options, launches the an interactive shell to interact with the script in order to choose specific options, including the *domain name*, *realm name*, *admin* and *directory admin* passwords, among other definitions, like DNS and CA certification management.

The setup creates an instance and configures all the services and policies:
- 389 LDAP Directory Server instance
- Kerberos Key Distribution Center (KDC)
- Active Directory WinSync plug-in
- Certificate Authority (CA)
- Network Time Protocol Daemon (ntpd)
- Apache (httpd)
- SELinux targeted policy
- Domain Name Service (DNS) - optional

The script can also be run non-interactively, especifying the **-U** option among the other required options. The full list of options can be found in the **ipa-server-install** man page.

#### CA Configuration
The Dogtag Certificate System comes with IdM and can be used to create all the required certificates and keytab used by users and hosts in the domain.

By default, IdM servers are installed with the certificate system being managed by the Dogtag Certificate System. Within the domain, the CA uses a signing certificate to create and sign all of the host and user certificates. On the IdM itself, server certificates are required for the internal domain to work properly, e.g., for the LDAP server and the Apache server (for the IdM Management UI) to establish secure connections with each other.

For the signing certificate to work, the CA certificate has to be signed by the CA that issued it. There are two ways that a CA can sign the Dogtag Certificate System CA signing certificate:
- The default Identity Management configuration uses it to sign its own certificate. There are no additional parameters or configuration steps required when the installation is run. This configuration means that there's no higher CA instance and that the IdM's Certificate System is its own CA.
- The initial domain CA certificate is issued by a different, and possibly externally-hosted CA. In this configuration, the external CA is the root CA for the IdM domain while the IdM CA is subordinate to the external root CA. This means that attributes such as validity period are set by the Root CA. In order to use an external CA, a CSR is generated to the external CA and then both the CA certificate along with the issued server certificate must be loaded to complete the setup.

Although it's possible to also install IdM without a CA, this requires that all certificates used in the IdM domain to be created, uploaded and renewed manually.

**IMPORTANT**

Note that it's not possible to change the CA configuration after installation.

#### DNS Configuration
DNS must be properly configured for the IdM domain functions to work properly. When using the integrated DNS, most of the DNS record maintenance is automated, but it should be noted that the integrated DNS provided by IdM is not designed to be used as a general-purpose DNS server, since its main function is to support IdM deployment and maintenance, thus it doesn't support advanced DNS features.

IdM can also function without the integrated DNS, but all the necessary entries should then be managed manually:
- The new domain must be created on the external DNS server
- The entries should be manually populated with the new domain records from the zone file generated by the IdM installer
- The records should be updated any time a replica is installed
- The records should be updated after any changes in service configuration, such as when an Active Directory trust has been configured.

#### NTP Configuration
Since many IdM services, like Kerberos, require synchronized clocks to work, IdM can serve also as an NTP server for the domain, and it will synchronize time and date before any operation can be performed.

