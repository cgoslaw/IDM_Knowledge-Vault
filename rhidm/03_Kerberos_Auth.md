# Configuring Services and Hosts for Kerberos
Usually, Kerberized services should use a dedicated service principal, although some legacy configurations still reuse the host principal, like SSH who usually uses the host principal as its identifier when a user tries to access the host - in this case the host principal identifies the specific machine and for SSH this is all that is required.

A service principal is preferable due to the service could be run on more than one machine, or even a friendly name may be preferred over the hostname. Usually, the service principals follow the format **service/hostname@REALM**. The service should match the service being authenticated, such as HTTP, NFS or LDAP. Since there's no name resolution in Kerberos, the host name is used in the instance position, thus a Kerberized webserver could have a principal like: ***HTTP/webserver.example.com@EXAMPLE.COM**.

Differently from users who may input their credentials upon a prompt, host and service principals are not able to and due to this reason their credentials have to be stored in a specific file called a **keytab**, usually located in the **/etc/krb5.conf** path for a host principal. However, service principal keytabs can be stored anywhere that service has read permissions and this allows these services to start up without any human intervention.

## Configuring Service Principals.
Host principals are created automatically when a host joins a Kerberized domain like IdM. The host principal is then downloaded and stored in the default path.

Differently, service principals can be created through the IdM web UI by navigating to **Identity -> Services**. The service principals can also be created from the CLI with the **ipa service-add** command and later the keytab can be retrieved by running the **ipa-getkeytab**.
E.g.,
```
# ipa-getkeytab -s idm-srv01.example.com -p HTTP/webserver.example.com -k /etc/httpd/conf/webserver.keytab
```
## Managing Host and Service Principals
There are times, however, when it's needed to temporarily disable access to a host or service. When a service or host principal is disabled, their keytab attribute is set to **False** However, for the service to be enabled again, it's necessary to request a new keytab.
```
# ipa service-disable HTTP/webserver.example.com
```

It's also possible to delegate service management to a service owner, so that whenever that defined user needs to manage their service (disable and renew keytabs, for example), they are able to retrieve and renew an update keytab by themselves instead of burdening an IdM administrator.

For that, the user must be authenticated using the host keytab file:
```
# kinit -kt /etc/krb5.keytab host/$(hostname)
```
# Managing the Kerberos Domain
The Kerberos protocol uses AES symmetric encryption. Each principal has a single key stored in the KDC database, which in case of user principals is derived from their passwords. That means that when the password is changed, a new key is also generated and as result, the key hash also changes.

**NOTE** that password and key hashes are not exposed through the *IdM API*, and can only be seen using the *Directory Manager* user for the back-end LDAP directory.

## Kerberos Ticket Policies
When a user authenticates to the IdM domain, a ticket with a certain time to live is issued. This time period is configurable at both global or user-specific level. Note that a user-specific policy always overrides the global ticket policy.

Both ticket policies are modified using the same command. If a user id is included, the command affects only that specified user, otherwise the modification applied will be global:
```
# ipa krbtpolicy-mod [userid] --maxrenew=<seconds> --maxlife=<seconds>
```
## Kerberos Flags on Service Principals
There are several flags that can be set on service principals, although most common flags are related to delegation:
- **OK_AS_DELEGATE** is used when a service is trusted to have a client's TGT forwarded to it. In IdM, this is usually used when the IdM domain has a trust relationship with *Active Directory*, which will forward the user's TGT to the service if the flag is set.
- **OK_TO_AUTH_AS_DELEGATE** is used when a service is trusted to authenticate as the client. Note that this means that the service may run without higher privileges and impersonate clients to perform work on their befalf.
- **REQUIRES_PRE_AUTH** is usually enabled by default and can be used to disable pre-authentication for a specific service. This reduces the load on the KDC but also reduces the security of the service credential. The pre-authentication feature was introduced to mitigate a weakness in the Kerberos protocol that allowed an attacker to perform offline password cracking. Note that it's recommended to never disabe pre-auth.
```
# ipa service-mod HTTP/webserver.example.com@EXAMPLE.COM --ok-as-delegate=1
```
In order to see the current flags for a service principal, the **kadmin.local** command can be used:
```
# kadmin.local
Authenticating as principal admin/admin@EXAMPLE.COM with password.
kadmin.local: getprinc HTTP/webserver.example.com@EXAMPLE.COM
```
## Rekeying Service and Host Principals
Differently from users who can change their passwords which will then change their secret keys, host and service principal credentials have randomly generated keys when they are created, which are not subject to password expiry. For this reason, a keytab file is a sensitive item that must be protected like a user's credential, as gaining access to a keytab allows anyone to impersonate the host or service. In case a keytab is suspected of being exposed, then a new key cna be requested. This process of requesting a new key from the KDC is called **Rekeying**, which in turn will increase the **key version number** (**kvno**).

The process for rekeying is the same as requesting a new keytab:
```
# ipa-getkeytab -p HTTP/webserver.example.com@EXAMPLE.COM -s idm-srv01.example.com -k /etc/httpd/conf/webserver.keytab
```
Using *klist* with the *kt* options allows the user to verify the keytab's kvno:
```
# klist -kt /etc/httpd/conf/webserver.keytab
Keytab name: FILE:/etc/httpd/conf/webserver.keytab
KVNO  TIMESTAMP         PRINCIPAL
----  ----------------- ------------------------------------------
   2  07/02/18 09:21:33 HTTP/webserver.example.com@EXAMPLE.COM
   3  12/02/18 16:41:12 HTTP/webserver.example.com@EXAMPLE.COM
```
**NOTE** that the previous key version is still in the keytab to allow existing sessions to expire gracefully. New sessions, however, will use the highest version key.
