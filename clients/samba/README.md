# What is Samba
Samba is a suite of software that runs on Unix platforms and speaks to Windows clients like a native client. It allows a Unix system to move into a Windows "Network Neighbourhood" without causing issues, and Windows users can normally access file and print services without knowing or caring that the services being offered are hosted on a Unix host.
This is managed through a protocol suite which is currently known as the **Common Internet File System**, or **CIFS**. At the heart of CIFS is the latest incarnation of the **Server Message Block** protocol (**SMB**). Samba is an open source CIFS implementation.

The origin of Samba began with the **Net**work **B**Asic **I**nput **O**utput **S**ystem, **NetBIOS**, a piece of software developed by IBM and Sytec that was loaded into memory to provide an interface between programs and the network hardware. It included an addressing scheme that used 16-byte names to identify workstations and network-enabled applications.

Later, Microsoft added features to DOS that allowed disk I/O to be redirected to the NetBIOS interface, which made disk space shareable over the LAN. The file-sharing protocol that was used eventually became known as SMB, and now CIFS.

Many other software was also written to use the NetBIOS API - IBM introduced the NetBEUI (NetBios Enhanced User Interface), that provided a mechanism for passing NetBIOS packets over Token Ring and Ethernet. Other developed NetBIOS LAN emulation over higher-level protocols, including DECnet, IPX/SPX and TCP/IP.

NetBIOS and TCP/IP worked well together. The latter could be routed between interconnected networks (internetworks), but NetBIOS was designed for isolated LANs. The trick was to map the 16-byte NetBIOS names to IP addresses so that messages could actually find their way through a routed IP network. A mechanism for doing that was described in the Internet RFC1001 and RFC1002 documents. And as Windows evolved, Microsoft added two additional pieces to the SMB package. These were service announcement, called **browsing** and a central authentication and authorization service, known as the Windows NT Domain Control.

Later, around 1992, Andrew Tridgell had to mount a disk space from a Unix server on a DOS, but also required an application that required NetBIOS. Using a packet sniffer he wrote, he reverse engineered the SMB protocol and implemented in the UNIX box. After a while, this piece of code became **Samba**.
# Samba Features
Samba consists of two key programs - **smbd** and **nmbd**, along with other pieces of software. The job of both programs is to implement the four basic modern-day CIFS services, which are:
- File & print services
- Authentication and Authorization
- Name resolution
- Service announcement (browsing)
Samba also implements the Distributed Computing Environment Remote Procedure Call (*DCE RPC*) protocol used by Microsoft Windows.

# Samba in RHEL systems
In RHEL, Samba can be run as:
- An Active Directory or NT4 domain member
- A standalone Server
- A NT4 Primary Domain Controller (PDC) or a Backup Domain Controller (BDC)
*Note that Red Hat supports these last modes only in existing installations with Windows versions which support NT4 domains. It's not recommended to set up new NT4 domains, as Microsoft operating systems later than Windows 7 and Windows Server 2008 R2 do not support NT4 domains*.
Independently of the installation mode, directories and printers can be shared. This enables Samba to act as a file and print server.

Samba's main configuration file is the **/etc/samba/smb.conf**.
## The Samba Services
### smbd
The **smbd** service provides file sharing and printing services using the SMB protocol. This service is also responsible for resource locking and authenticating users. In RHEL systems, the service comes with the **samba** package.
### nmbd
The **nmbd** service provides hostname and IP resolution using NetBIOS over IPv4 protocol. Additionally to the name resolution, the service enables the service announcement (browsing) over the SMB network to locate domains, work groups, hosts file shares and printers. For this, the service either reports this information directly to the broadcasting client or forwards it to a local or master browser. Lately, however, SMB networks use DNS to resolve clients and IP addresses. In RHEL systems, the service also comes with the **samba** package.
### winbindd
The **winbindd** service provides an interface for the **Name Service Switch** (**NSS**) to use AD or NT4 domain users and groups on the local system. This enables domain users to authenticate to services hosted on the Samba server, or to other local services.

if Samba is set up as a domain member to an Active Directory domain, **winbindd** must be started before the **smbd** service. Otherwise, domain users and groups will not be available to the local system.

It's important to note, that Red Hat only supports Samba as a server with the **winbindd** service to provide domain users and groups to the local system. SSSD is not supported with Samba since it lacks ACL and NT LAN Manager fallback support.

## The Samba Security Modes
The **security** parameter in the **[global]** section in the configuration file manages how Samba authenticates users that are connecting to the service. Depending on the mode Samba is installed, the parameter must be set to different values.
### Active Directory Security Mode
The Active Directory Security Mode is set when the host is joined to the domain as a native Active Directory member. Even if a security policy restricts the use of NT-compatible authentication protocols, the Samba server can join an ADS using Kerberos. Samba in Active Directory member mode can accept Kerberos tickets, and uses Kerberos to authenticate AD users.
The mode is set with the following parameter:
```
security = ADS
```
### User-Level Security
The User-level security is the default setting for Samba, even if the *security* parameter is not defined in the configuration. If the server accepts the client's username/password, the client can mount multiple shares without specifying a password for each instance. Samba can also accept session-based username/password requests. The client maintains multiple authentication contexts by using a unique UID for each logon. This uses a local database to authenticate the users and works only as a standalone server.
The mode is set with the following parameter:
```
security = user
```
### Domain Security mode
In the domain security mode, the Samba server has a machine account (domain security trust account) and causes all authentication requests to be passed through the domain controllers. 
This mode, however, cannot be used on AD domain members, only on NT4 PDC or BDC.
The mode is set with the following parameter:
```
security = domain
```
## ID Mapping & Samba Back End Databases
### ID Mapping
In Windows, users and groups are distinguished by unique **Security Identifiers** (**SID**), whereas in Linux, they are distinguished by unique UIDs and GIDs. If Samba is run as a domain member, the **winbindd** service is responsible for providing the information about domain users and groups to the operating system.
In order to enable the **winbdd** service to provide unique IDs for users and groups to Linux, the ID mapping must be configured for:
- The local database (default domain)
- The AD or NT4 domain the Samba server is a member of
- Each trusted domain from which users must be able to access resources on this Samba server
Regardless of whether the Linux UIDs and GIDs are stored in the Active Directory or if Samba will generate them, each domain requires a range ID that *must not* overlap with any other configured domains, and only one range can be assigned per domain. Therefore, enough space should be left between domain ranges, as this enables the domain ranges to be extended later on in case the domain grows. Otherwise, if another range is assigned to the domain, the ownership of files and directories will be lost.

Also, in a domain environment, one ID mapping configuration can be added for each of the following:
- The domain the Samba server is a member of
- Each trusted domain that should be able to access the Samba server
However, for all other objects, Samba assigns IDs from the default domain, and this includes:
- Local Samba users and groups
- Samba built-in accounts and groups, such as BUILTIN\Administrators
The default domain back end must be writable to permanently store the assigned IDs.

Example of an ID mapping range configuration with two domains, and the default domain:
```
[global]
...
idmap config * : backend = tdb
idmap config * : range = 5000 - 6999

idmap config AD-DOM:backend = ad
idmap config AD-DOM:range = 8000 - 9999

idmap config TRUST-DOM:backend = rid
idmap config TRUST-DOM:range = 10000 - 11999
...
```
### Back end Databases
Samba can work with some different database types for each specific purposes.
#### tdb
The tdb back end is mostly exclusively used with the * default domain only. The **winbindd** service uses the writable **tdb** ID mapping back end by default to store the SID, UID and GID mapping tables. It includes local users, groups and built-in principals. When the default domain is configured, it's always recommended to set an ID range that is big enough to include objects that will be created in the future and are not part of a defined domain ID mapping configuration
```
[global]
...
idmap config * : backend = tdb
idmap config * : range = 10000 - 999999
...
```
#### ad
The **ad** mapping back end implements a read-only API to read account and group information from AD. This provides some benefits such as:
- All user and group settings are stored centrally in the Active Directory.
- User and group IDs are consistent on all Samba servers that use this back end.
- The IDs are not stored in a local database which can be corrupt, and therefore file ownerships cannot be lost.

The ad back end reads the following attributes from the Active Directory:
```
+===================+=================+=============================================+
| AD attribute name |   Object type   | Mapped to                                   |
|===================================================================================+
| sAmaccountName    | User and group  | User or group name, depending on the object |
|-----------------------------------------------------------------------------------+
| uidNumber         | User            | User ID (UID)                               |
|-----------------------------------------------------------------------------------+
| gidNumber         | Group           | Group ID (GID)                              |
|-----------------------------------------------------------------------------------+
| loginShell¹       | User            | Path to the shell of the user               |
|-----------------------------------------------------------------------------------+
| unixHomeDirectory¹| User            | Path to the home directory of the user      |
|-----------------------------------------------------------------------------------+
| primaryGroupID²   | User            | Primary group ID                            |
+-----------------------------------------------------------------------------------+
[1] Samba only reads this attribute if set **idmap config DOMAIN:unix_nss_info = yes**
[2] Samba only reads this attribute if set **idmap config DOMAIN:unix_primary_group = yes**
```
This back end have some prerequisites, such as:
- Both users and groups must have unique IDs set in AD, and the IDs must be within the range configured in the configuration file. Objects whose IDs are outside of the range will not be available on the Samba server.
- Users and groups must have all required attributes set in AD. If required attributes are missing, the user or group will not be available on the Samba server.
```
[global]
# Default domain to use tdb and a big range
idmap config * : backend = tdb
idmap config * : range = 10000-99999

# Configuring backend, range and schema-mode as per RFC2307
idmap config AD-DOM:backend = ad
idmap config AD-DOM:range = 200000 - 299999
idmap config AD-DOM:schema-mode = rfc2307

# Enabling Samba to read the login shell and the path to homedir
idmap config AD-DOM:unix_nss_info = yes  

# Alternatively, domain-wide homedir and login shell can be applied:
# template shell = /bin/bash
# template homedir = /home/%D/%U

# Setting Samba to use the gidNumber instead of the primaryGroupID for users' primary group
idmap config AD-DOM:unix_primary_group = yes
```
#### rid
The rid ID mapping back end implements a read-only API to calculate account and group information based on an algorithmic mapping scheme for AD and NT4 domains. The RID is the last part of a SID. For example, if the SID of a user is **S-1-5-21-5421822485-1151247151-421485315-30014**, then the RID is **30014**.When you configure the back end, the lowest and highest RID range parameter must be set. Samba will not map users or groups with a lower or higher RID than set in this parameter.

As a read-only back end, rid cannot assign new IDs, such as for BUILTIN groups. Therefore, this back end should not be used for the default * domain.

Some benefits of using rid are:
- All domain users and groups that have an RID within the configured range are automatically available on the domain member.
- There's no need to manually assign IDs, home directories and login shells
However, some of the drawbacks are:
- All domain users get the same login shell and home directory assigned. Though it's possible to circunvent this with variables.
- User and group IDs are only the same across Samba domain member if all use the rid back end with the same ID range settings.
- It's not possible to exclude individual users and groups from being available on the domain member. Only users and groups outside the configured range are excluded
- Based on the formules the **winbindd** service uses to calculate IDs, duplicate IDs can occur in multi-domain environments if objects in different domains have the same RID.
```
idmap config * : backend = tdb
idmap config * : range = 10000-99999

idmap config DOMAIN : backend = rid
idmap config DOMAIN : range = 200000 - 299999

template shell = /bin/bash
template homedir = /home/%D/%U
```
#### autorid
The **autorid** back end works similarly to the **rid** ID mapping back end, but can automatically assign IDs for different domains. This allows using **autorid** for the default * domain, as well as additional domains without the need to create an ID mapping configuration for each of the additional domains.

If the **autorid** is used for the default domain, adding additional ID mapping configuration for domains becomes optional.

Some of the benefits of using the autorid are:
- All the domain users and groups whose calculated UID and GID is within the configured range are automatically available on the domain member.
- There's no need to manually assign IDs, home directories and login shells.
- No duplicate IDs, even if multiple objects in a multi-domain environment have the same RID.
However, some of the drawbacks are:
- User and group IDs are not the same across Samba domain members.
- All domain users get the same login shell and home directory assigned. However, variables can also be used like with **rid**.
- Individual users or groups cannot be excluded from being available from the domain member. Only users and groups whose calculated UID or GID is outside of the configured range are excluded.
```
idmap config * : backend = rid
idmap config * : range = 10000-999999

# Range size can be set so Samba assigns only the specified number of continuous IDs for each domain's object until all IDs from the range set in the range parameter are taken.
idmap config * : rangesize = 20000

template shell = /bin/bash
template homedir = /home/%D/%U
```


